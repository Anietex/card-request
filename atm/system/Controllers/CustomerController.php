<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 7/11/2018
 * Time: 12:25 PM
 */

namespace Controllers;

use Model\CustomerModel;
use Utils\FileUpload\FileUploader;
class CustomerController
{
    public function createAccount(){
        $customer = new CustomerModel();
        $customer->first_name = $_POST['first_name'];
        $customer->last_name = $_POST['last_name'];
        $customer->middle_name = $_POST['middle_name'];
        $customer->gender  = $_POST['gender'];
        $customer->date_of_birth = $_POST['dob'];
        $customer->occupation = $_POST['occupation'];
        $customer->mobile1 = $_POST['mobile_one'];
        $customer->mobile2 = $_POST['mobile_two'];
        $customer->email = $_POST['email'];
        $customer->address = $_POST['home_address'];
        $customer->address_landmark = $_POST['landmark'];
        $customer->state = $_POST['state'];
        $customer->city = $_POST['city'];
        $customer->identification_type = $_POST['id_type'];
        $customer->id_number = $_POST['id_number'];
        $customer->date_issued = $_POST['issue_date'];
        $customer->expiry_date = $_POST['expiry_date'];
        $customer->opening_balance = $_POST['balance'];
        $customer->question1 = $_POST['question1'];
        $customer->answer1 = $_POST['answer1'];
        $customer->question2 = $_POST['question2'];
        $customer->answer2 = $_POST['answer2'];
        $customer->account_no = $this->genAccountNo();

        $errors = [];
        $filename = '';
        $uploader = new FileUploader($_FILES['passport']);
        $uploader
            ->setAcceptedExtensions(['jpg','png','gif','jpeg'])
            ->setMaxSize(2000)
            ->setUploadPath('uploads/passports');

        if($uploader->validateSize()){
            if($uploader->validateExtension()){
                if($uploader->uploadFile()){
                    $filename = $uploader->getUploadedFileName();
                    $customer->passport_file =$filename;
                    $customer->store();
                    return json_encode(['status'=>'success']);
                }
            }else{
               array_push($errors,"Passport must be an image file");
            }
        }else{
            array_push($errors,"Passport file must not be greater than 2MB");
        }
        return json_encode(['status'=>'error','errors'=>$errors]);
    }


    public function showAccounts(){
        $customers = new CustomerModel();
        return json_encode($customers->all());
    }

    public function showAccount($id){
        $customer = new CustomerModel($id);
        $data['id'] = $id;
        $data['first_name'] = $customer->first_name;
        $data['last_name'] =$customer->last_name;
        $data['middle_name'] =$customer->middle_name;
        $data['gender'] =$customer->gender;
        $data['date_of_birth'] =$customer->date_of_birth;
        $data['occupation'] =$customer->occupation;
        $data['mobile1'] =$customer->mobile1;
        $data['mobile2'] =$customer->mobile2;
        $data['email'] =$customer->email;
        $data['address'] =$customer->address;
        $data['landmark'] =$customer->address_landmark;
        $data['state'] =$customer->state;
        $data['id_type'] =$customer->identification_type;
        $data['id_number'] =$customer->id_number;
        $data['date_issued'] =$customer->date_issued;
        $data['expiry_date'] =$customer->expiry_date;
        $data['opening_balance'] =$customer->opening_balance;
        $data['question1'] =$customer->question1;
        $data['answer1'] =$customer->answer1;
        $data['question2'] =$customer->question2;
        $data['answer2'] =$customer->answer2;
        $data['city'] = $customer->city;
        $data['account_number'] = $customer->account_no;
        $data['passport'] = $customer->passport_file;
        return json_encode($data);
    }


    public function update(){
        $customer = new CustomerModel($_POST['id']);
        $customer->first_name = $_POST['first_name'];
        $customer->last_name = $_POST['last_name'];
        $customer->middle_name = $_POST['middle_name'];
        $customer->gender  = $_POST['gender'];
        $customer->date_of_birth = $_POST['dob'];
        $customer->occupation = $_POST['occupation'];
        $customer->mobile1 = $_POST['mobile_one'];
        $customer->mobile2 = $_POST['mobile_two'];
        $customer->email = $_POST['email'];
        $customer->address = $_POST['home_address'];
        $customer->address_landmark = $_POST['landmark'];
        $customer->state = $_POST['state'];
        $customer->city = $_POST['city'];
        $customer->identification_type = $_POST['id_type'];
        $customer->id_number = $_POST['id_number'];
        $customer->date_issued = $_POST['issue_date'];
        $customer->expiry_date = $_POST['expiry_date'];
        $customer->opening_balance = $_POST['balance'];
        $customer->question1 = $_POST['question1'];
        $customer->answer1 = $_POST['answer1'];
        $customer->question2 = $_POST['question2'];
        $customer->answer2 = $_POST['answer2'];
      //  $customer->account_no = $this->genAccountNo();

        $errors = [];
        $filename = '';
        if($_FILES['passport']['size']>0){
            $uploader = new FileUploader($_FILES['passport']);
            $uploader
                ->setAcceptedExtensions(['jpg','png','gif','jpeg'])
                ->setMaxSize(2000)
                ->setUploadPath('uploads/passports');
            if($uploader->validateSize()){
                if($uploader->validateExtension()){
                    if($uploader->uploadFile()){
                        $filename = $uploader->getUploadedFileName();
                        $customer->passport_file =$filename;
                    }
                }else{
                    array_push($errors,"Passport must be an image file");
                }
            }else{
                array_push($errors,"Passport file must not be greater than 2MB");
            }
        }

        if(count($errors)>0){
            return json_encode(['status'=>'error','errors'=>$errors]);
        }
        $customer->store();
        return json_encode(['status'=>'success']);

    }


    public function deleteCustomer($id){
        CustomerModel::delete($id);
        return json_encode(['status'=>'success']);
    }


    private function genAccountNo(){
        $num = '';
        for($i=0; $i<10; $i++){
            $num.=mt_rand(0,9);
        }

        return $num;
    }


}