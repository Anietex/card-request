<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 7/12/2018
 * Time: 3:20 PM
 */

namespace Controllers;


use Database\Model;
use Model\CustomerModel;
use Model\RequestModel;

class CardRequestController
{
    public function getCustomerInfo($accountNo){
        $customer = new CustomerModel();

        $qb =$customer->getQueryBuilder();

        $cus = $qb->select('id','first_name','last_name','middle_name','account_no')
            ->from('customers')
            ->where('account_no',$accountNo)
            ->first()
            ->get();

        return json_encode($cus);
    }


    public function getRequests(){
        $request  = new RequestModel();


        return json_encode($request
            ->getQueryBuilder()
            ->select('card_request.*','first_name','last_name','middle_name')
            ->from('card_request')
            ->innerJoin('customers','customers.id = card_request.customer_id')
            ->get()
        );
    }


    public function getSecurityQuestion(){
        $customer = new CustomerModel($_GET['id']);

        $data['question1']=$customer->question1;
        $data['question2']=$customer->question2;

        return json_encode($data);
    }

    public function handleCardRequest(){
        $cus  = new CustomerModel($_POST['id']);
        $req = new RequestModel();
        $errors ='';
        $QB = $req->getQueryBuilder();

        $r = $QB->query('select * from card_request where customer_id = ? and  status <> ?',[$_POST['id'],'Completed']);


        if(count($r)>0)
          return json_encode(['status'=>'error','error'=>'You Have already sent a request it\'s yet to be completed']);


        if($cus->answer1 == $_POST['answer1'] &&  $cus->answer2 == $_POST['answer2']){
            if($cus->opening_balance>=6000){
                $tc ='TK'.mt_rand(1000,9999);
                $req->customer_id = $_POST['id'];
                $req->card_type = $_POST['card_type'];
                $req->reason = $_POST['reason'];
                $req->tracking_code = $tc;
                $req->requested_on = date('Y-m-d');
                $req->store();
                return json_encode(['status'=>'success','tracking_code'=>$tc]);
            }else{
                $errors = "Insufficient funds";
            }
        }else{
            $errors = 'Wrong answer to security question';
        }

        return json_encode(['status'=>'error','error'=>$errors]);
    }


    public function updateRequest(){
        $re  = new RequestModel($_POST['id']);
        $re->status = $_POST['status'];
        $re->store();
        return json_encode(['status'=>'success']);

    }

    public function getRequestStatus(){
        $re = new RequestModel();
        $QB = $re->getQueryBuilder();
        $status = $QB->select()
            ->from('card_request')
            ->where('tracking_code',$_GET['tracking_code'])->first()->get();

        if($status==null){
            return json_encode(['status'=>'error','message'=>'Tracking Code does not exist']);
        }
        if($status->status == 'Completed')
            $msg = 'Your card request is completed';

        if($status->status == 'Being Processed')
            $msg = 'Your card request is currently being processed';
        if($status->status == 'Unprocessed')
            $msg = 'Your card request is yet to be being processed';

        if($status->status == 'Being Shipped')
            $msg = 'Your card request is currently being shipped to you';

        if($status->status == 'Rejected')
            $msg = 'Your card request is was rejected';

        return json_encode(['status'=>'success','message'=>$msg]);
    }


}