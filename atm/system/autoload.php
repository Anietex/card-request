<?php

$roots = ['system'];
spl_autoload_register(function ($class) use($roots){
    foreach ($roots as $root){
        if(file_exists($root.DIRECTORY_SEPARATOR.$class.'.php'))
            require $root.DIRECTORY_SEPARATOR.$class.'.php';
    }
});