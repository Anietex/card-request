<?php

namespace Model;


use Database\Model;

class Admin extends Model
{
    protected $primaryKey = 'id';
    protected $table = 'admin';
}