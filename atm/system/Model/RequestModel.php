<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 7/13/2018
 * Time: 8:09 AM
 */

namespace Model;


use Database\Model;

class RequestModel extends Model
{
    protected $table = 'card_request';
    protected $primaryKey = 'id';
}