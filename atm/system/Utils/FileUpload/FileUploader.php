<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 7/11/2018
 * Time: 10:58 AM
 */
namespace Utils\FileUpload;
class FileUploader
{
    private $file;
    private $uploadPath;
    private $extensions = [];
    private $mimes;
    private $fileName;
    private $minSize = 1;
    private $maxSize = 0;
    private $errors = [];




    public function __construct($file)
    {
        if($file['size'] == 0)
            throw new \InvalidArgumentException('Invalid file or no file was selected');
        $this->file = $file;

    }


    public function validateExtension(){
        $ext = $this->getFileExtension();
        if(count($this->extensions)>0)
            if(!in_array($ext,$this->extensions))
                return false;
        return true;
    }

    public function validateSize(){
        if($this->maxSize>0)
           if($this->file['size']>$this->maxSize*1024)
                return false;

        return true;
    }


    public function uploadFile($name=''){
        if($name=='')
            $name = sha1_file($this->file['tmp_name']).'.'.$this->getFileExtension();
        $file = $this->getUploadPath().'/'.$name;
        $r = move_uploaded_file($this->file['tmp_name'],$file);
        $this->errors = $this->file['error'];
        $this->fileName = $name;
        return $r;
    }

    public function getFileName(){
        return $this->file['name'];
    }

    public function getUploadedFileName(){
        return $this->fileName;
    }



    /**
     * @return mixed
     */
    public function getMinSize()
    {
        return $this->minSize;
    }

    /**
     * @param mixed $minSize
     */
    public function setMinSize($minSize)
    {
        $this->minSize = $minSize;
    }

    /**
     * @return mixed
     */
    public function getMaxSize()
    {
        return $this->maxSize;
    }

    /**
     * @param mixed $maxSize
     */
    public function setMaxSize($maxSize)
    {
        $this->maxSize = $maxSize;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getFile()
    {
        return $this->file;
    }

    public function getFileSize(){
        return $this->file['size'];
    }

    /**
     * @return mixed
     */
    public function getFileExtension()
    {
        $info = new \SplFileInfo($this->file['name']);

        return $info->getExtension();
    }


    /**
     * @return mixed
     */
    public function getUploadPath()
    {
        return $this->uploadPath;
    }

    /**
     * @param mixed $uploadPath
     * @return FileUploader
     */
    public function setUploadPath( $uploadPath)
    {
        $this->uploadPath = stripcslashes($uploadPath);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAcceptedExtensions()
    {
        return $this->extensions;
    }

    /**
     * @param mixed $extensions
     * @return FileUploader
     */
    public function setAcceptedExtensions(array $extensions)
    {
        $this->extensions = $extensions;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMimes()
    {
        return $this->mimes;
    }

    /**
     * @param mixed $mimes
     * @return FileUploader
     */
    public function setMimes($mimes)
    {
        $this->mimes = $mimes;
        return $this;
    }



    /**
     * @param mixed $files
     * @return FileUploader
     */
    public function setFile($file)
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @param mixed $fileName
     * @return FileUploader
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;
        return $this;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
}