const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

module.exports ={
    entry:{
        user:'./src/js/user/index.js',
        admin:'./src/js/admin/index.js'
    },
    output: {
        filename: "[name].bundle.js",
        path: path.resolve(__dirname,'dist/js'),
    },
    module: {
        rules: [
            {
                test:/\.vue$/,
                use: "vue-loader"
            },
            {
                test: /\.s[a|c]ss$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test:/\.js$/,
                use:'babel-loader'
            }
        ],

    },

    mode: "development",


    plugins: [
        new VueLoaderPlugin(),
    ]
};