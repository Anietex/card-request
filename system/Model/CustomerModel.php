<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 7/11/2018
 * Time: 12:25 PM
 */
namespace Model;
use Database\Model;
class CustomerModel extends Model
{
   protected $primaryKey = 'id';
   protected $table = 'customers';
}