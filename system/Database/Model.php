<?php


namespace Database;


class Model
{
   protected $table = '';
   protected $primaryKey = 'id';
   private $db = null;
   private $currentId = null;
   private $columns = [];
   private $currentRecord = null;


   public function __construct($id=null)
   {
       $this->db = new Database($this);
       if(isset($id)){
           $this->currentId = $id;
           $this->getCurrentRecord();
       }
       $this->getColumns();


   }


   public function all(){
     return  $this->db->getRows();
   }

    public function getQueryBuilder(){
        return $this->db->getQueryBuilder();
    }


   public function getID(){
       return $this->currentId;
   }

   public function getTable(){
       return $this->table;
   }

   private function getColumns(){
       $columns= $this->db->getColumns();
       foreach ($columns as $column){
           array_push($this->columns,$column);
       }
   }

   public function getCurrentRecord(){
       $this->currentRecord = $this->db->getRow($this->currentId);
   }



   public function getPrimaryKey(){
       return $this->primaryKey;
   }

   public function store(){
        if($this->currentRecord ==null){
            $r = $this->db->insert();
            $this->currentId = $this->db->getLastInsertId();
            return $r;
        }else{
            return  $this->db->update();
        }
   }


   public function destroy(){
       return $this->db->delete();
   }

   public static function delete($id){
       $class =get_called_class();
       $model = new $class($id);
       return $model->destroy();
   }

   public static function QB():QueryBuilder{
       $class = get_called_class();
       $model = new $class();
       return $model->getQueryBuilder();
   }


   public static function find($id){
       $class =get_called_class();
       return  new $class($id);
   }

    public function getLastInsertId(){
        return $this->db->getLastInsertId();
    }


   public function __set($name, $value)
   {
       if(in_array($name,$this->columns)){
            $this->db->setColumnKeyValue($name,$value);
       }else{
           trigger_error('Column or Property does not exist '.__CLASS__.'::$'.$name);
       }

   }

   public function __get($name)
   {
       if(in_array($name,$this->columns))
           return $this->currentRecord->$name;
       trigger_error('Column or Property does not exist '.__CLASS__.'::$'.$name);
       return null;
   }


}