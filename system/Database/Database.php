<?php
namespace Database;


class Database
{   private $dbh =null;
    private $QB = null;
    private $model = '';
    private $recordKeyValue = [];
    private $table = '';

    public function __construct(Model $model)
    {
        $this->model = $model;
        $this->table = $model->getTable();
        $this->dbh = $this->getConnection();
        $this->QB = new QueryBuilder($this->dbh);
    }

    private function getConnection(){
        return require 'connection.php';
    }

    public function getDBH(){
        return $this->dbh;
    }

    public function insert(){
        return $this
            ->QB
            ->insert($this->recordKeyValue)
            ->into($this->table
            )->execute();
    }

    public function update(){
       return $this
            ->QB
            ->where($this->model->getPrimaryKey(),$this->model->getID())
            ->set($this->recordKeyValue)
            ->update($this->table)->execute();
    }


    public function delete(){
        return $this
            ->QB
            ->where($this->model->getPrimaryKey(),$this->model->getID())
            ->delete($this->table)->execute();
    }



    public function setColumnKeyValue($key,$value){
        $this->recordKeyValue[$key] = $value;
    }

    public function getTableMetaData(){
        return $this->QB->describeTable($this->table);

    }

    public function getColumns(){
        $meta  = $this->getTableMetaData();
        $columns = [];
        foreach ($meta as $row){
            array_push($columns,$row['Field']);
        }
        return $columns;
    }


    public function getRow($id){

      return  $this->QB->select()
            ->from($this->table)
            ->where($this->model->getPrimaryKey(),$id)->first()
            ->get();

    }

    public function getLastInsertId(){
        return $this->QB->getLastInsertId();
    }

    public function getRows(){
        return  $this->QB->select()->from($this->table)->get();
    }

    public function getQueryBuilder(){
        return $this->QB;
    }



}