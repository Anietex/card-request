<?php


namespace Database;


class QueryBuilder
{
    protected $dbh = null;
    private  $whereClause ='';
    private $orderBy = '';
    private $columns = '';
    private $whereValues  = [];
    private $fetchType = 'rows';
    private $select ='';
    private $result = null;
    private $sql ='';
    private $table;
    private $insertData = [];
    private $updateData = [];
    private $values = [];
    private $join;
    private $order ='';
    private $limit='';
    private $lastInsertId;

    public function __construct(\PDO $dbh)
    {
        $this->dbh = $dbh;
    }

    public function insert(array $data){
        $this->insertData = $data;
        return $this;
    }


    public function update($table){
        $this->table = $table;
        $this->buildUpdate();
        return $this;
    }

    public function set($data){
        $this->updateData = $data;
        return $this;
    }

    public function delete($table){
        $this->table = $table;
        $this->buildDelete();
        return $this;
    }

    private function getColumns(){
        return $this->columns;
    }


    public function select(...$columns){
        if(count($columns)==0)
            $this->columns = '*';
        else
            $this->columns = implode(',',$columns);
        return $this;
    }




    public function execute(){
        $stmt =$this->dbh->prepare($this->sql);


       // print $this->sql;

        for($i =0; $i<count($this->values); $i++){
            $stmt->bindParam($i+1,$this->values[$i]);
        }
        $this->clearQuery();
        if(strpos(strtolower($this->sql),'insert')>-1)
            $this->lastInsertId = $this->dbh->lastInsertId();

        return $stmt->execute();
    }

    public function query($sql,array $params=[]){
        $stmt =$this->dbh->prepare($sql);
        $status =  $stmt->execute($params);
        $result =  $stmt->fetchAll(\PDO::FETCH_OBJ);

        if(strpos(strtolower($sql),'select')!==false)
            return $result;
        return $status;
    }

    private function clearQuery(){
        $this->join ='';
        $this->whereClause = '';
        $this->whereValues=[];
    }

    public function getLastInsertId(){
        return $this->lastInsertId;
    }



    public function all(){
        $this->fetchType = 'rows';
        return $this;
    }

    public function first(){
        $this->fetchType = 'row';
        return $this;
    }

    public function into($table){
        $this->table = $table;
        $this->buildInsert();
        return $this;
    }

    private function getTable(){
        return $this->table;
    }




    public function where($column,$value){
        if($this->whereClause =='')
            $this->whereClause = ' WHERE '.$column.' = ? ';
        else
            $this->whereClause .= ' AND WHERE '.$column.' = ? ';
        array_push($this->whereValues,$value);
        return $this;
    }


    public function orWhere($column,$value){
        if($this->whereClause =='')
            $this->whereClause = ' WHERE '.$column.' = ? ';
        else
            $this->whereClause .= ' OR WHERE '.$column.' = ? ';
        array_push($this->values,$value);
        return $this;
    }


    public function innerJoin($table,$on='',$where=''){
        $this->join.= 'INNER JOIN '.$table.' ON '.$on.' '.$where;
        return $this;
    }


    public function leftJoin($table,$on='',$where=''){
        $this->join.= 'LEFT JOIN '.$table.' ON '.$on.' '.$where;
        return $this;
    }


    public function rightJoin($table,$on='',$where=''){
        $this->join.= 'RIGHT JOIN '.$table.' ON '.$on.' '.$where;
        return $this;
    }

    public function orderBy($column,$order){
        $this->orderBy = " ORDER BY ".$column.' '.$order;
        return $this;
    }


    public function limit($limit,$start=''){
        if($start='')
            $this->limit = "LIMIT ".$limit;
        else
            $this->limit = "LIMIT ".$start.', '.$limit;
    }


    public function get(){
        $this->buildSelect();
        $stmt =  $this->dbh->prepare($this->sql);
        $stmt->execute($this->whereValues);

        $result = null;
        if($this->fetchType =='rows')
            $result = $stmt->fetchAll(\PDO::FETCH_OBJ);
        else
            $result = $stmt->fetch(\PDO::FETCH_OBJ);

        $this->clearQuery();
        return $result;
    }

    public function from($table){
        $this->table = $table;
        return $this;
    }



    private function buildInsert(){
        $data = $this->insertData;
        $columns  = array_keys($data);
        $this->values  = array_values($data);
        $placeholders = trim(str_repeat(', ?',count($data)),',');

        $this->sql = "INSERT ".$this->getTable().' ('.implode( ',',$columns). ') VALUES('.$placeholders.')';


    }


    private function buildUpdate(){
        $setSQL = ' SET ';
        $data = $this->updateData;
        $columns  = array_keys($data);
        $this->values  = array_values($data);

        array_map(function ($value){
            array_push( $this->values,$value);
        },$this->whereValues);

        foreach ($columns as $column){
            $setSQL .= $column .' = ? ,';
        }

        $this->sql = "UPDATE ".$this->table.rtrim($setSQL,',').$this->whereClause;
    }



    private function buildDelete(){
        array_map(function ($value){
            array_push( $this->values,$value);
        },$this->whereValues);

        $this->sql = "DELETE FROM ".$this->table.$this->whereClause;
    }


    private function buildSelect(){
        $this->sql = "SELECT ".$this->getColumns().' FROM '.$this->table.' '.$this->join.' '.$this->whereClause.$this->orderBy.' '.$this->limit;
    }

    public function describeTable($table){
        $stmt = $this->dbh->prepare('DESCRIBE '.$table);
        $stmt->execute();
        return $stmt->fetchAll(\PDO::FETCH_ASSOC);

    }
}