<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 7/13/2018
 * Time: 2:28 PM
 */

namespace Controllers;


use Model\Admin;

class AccountController
{

    public function getAccountInfo(){
        $admin = new Admin(1);
        $data['username'] = $admin->username;
        $data['password'] = $admin->password;
        return json_encode($data);
    }

    public function updateAccount(){
        $admin = new Admin(1);
        $admin->username = $_POST['username'];
        $admin->password = $_POST['password'];
        $admin->store();
        return json_encode(['status'=>'success']);
    }


    public function verifyAccount(){
        $admin = new Admin(1);
       return ($admin->username == $_POST['username'] &&
        $admin->password == $_POST['password']);
    }
}