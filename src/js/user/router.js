import Vue from 'vue'
import Router from 'vue-router'
import App from './App.vue'
import HomeMain from './HomeMain.vue';
import CardRequest from './CardRequest/Index.vue'
import AccountNumber from './CardRequest/AccountNumber.vue'
import AccountVerification from './CardRequest/AccountVerification.vue'
import CardPreference from './CardRequest/CardPreference.vue'
import SecurityQuestion from './CardRequest/SecurityQuestion.vue'
Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: HomeMain
    },
      {
          path:'/card/request',
          component:CardRequest,
          children:[
              {
                  path:'',
                  component:AccountNumber

              },
              {
                  path:'verification',
                  component:AccountVerification
              },
              {
                  path:'card_preference',
                  component:CardPreference
              },
              {
                  path:'security_question',
                  component:SecurityQuestion
              }
          ]
       }
  ]
})
