import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);
export default new Vuex.Store({
    state:{
        customer:{},
        preference:{}
    },
    mutations:{
        setCustomer(state,value){
            state.customer = value;
        },
        setPreference(state,value){
            state.preference = value;
        }
    }
})