import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import '../../../node_modules/materialize-css/sass/materialize.scss';

import '../../../node_modules/materialize-css/dist/js/materialize';


new Vue({
   el:'#admin',
   router,
    store,
    components:{ App },
    render:h=>h(App)
});
