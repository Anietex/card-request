import Vuex from 'vuex';
import Vue from 'vue';
import axios from 'axios';
Vue.use(Vuex);
export default new Vuex.Store({
    state:{
        customers:[],
        atmRequest:[],
        totalCustomers:0
    },
    mutations:{
        setCustomers(state,value){
            state.customers.length=0;
            state.customers.push(...value);
            state.totalCustomers = state.customers.length;
            console.log( state.totalCustomers)
        },

        setAtmRequest(state,value){

        }
    },

    getters:{
      getLength(state){
          return state.customers.length;
      }
    },
    actions:{
        getCustomers(context){

            return new Promise((accept,reject)=>{
                axios.get('api.php?request=get_customers')
                    .then(res=>{
                        const data = res.data;
                        context.commit('setCustomers',data)
                        accept(data)
                    });
            })

        }
    }
})



