import Vue from 'vue'
import Router from 'vue-router'
import Dashboard from './Dashboard.vue'
import AddCustomer from './AddCustomers.vue';
import Customers from './Customers.vue';
import CustomerDetails from './CustomerDetails.vue';
import EditCustomer from './EditCustomer.vue';
import CardRequest from './CardRequest.vue';
import Account from './AdminAccount.vue'
Vue.use(Router);
export default new Router({
    routes:[
        {
        path:'/',
        component:Dashboard
         },
        {
            path:'/customer/new',
            component:AddCustomer
        },
        {
            path:'/customers',
            component:Customers
        },
        {
            path:'/customer/:id',
            component:CustomerDetails,
        },
        {
            path:'/customer/edit/:id',
            component:EditCustomer
        },
        {
            path:'/card_requests',
            component:CardRequest
        },
        {
            path:'/account',
            component:Account
        }
    ]
})